const { exchangeRates } = require('../src/util');

module.exports = (app) => {

    app.post('/currency', (req, res) => {
       
        //2
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter NAME'
            });
        };
        //3
        if(typeof req.body.name != 'string'){
            return res.status(400).send({
                'error': 'Bad request: required parameter NAME is not a string'
            });
        };
        //4
        if(req.body.name.length === 0){
            return res.status(400).send({
                'error': 'Bad request: required parameter NAME is empty'
            });
        };
        //5
        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter EX'
            });
        };
        //6
        if(typeof req.body.ex != 'object'){
            return res.status(400).send({
                'error': 'Bad request: required parameter EXX is not an object'
            });
        };
        //7
        if(Object.keys(req.body.ex).length === 0){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter EX is empty'
            });
        };
        //8
        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad request: missing required parameter ALIAS'
            });
        };
        //9
        if(typeof req.body.alias != 'string'){
            return res.status(400).send({
                'error': 'Bad request: required parameter ALIAS is not a string'
            });
        };
        //10
        if(req.body.alias.length === 0){
            return res.status(400).send({
                'error': 'Bad request: required parameter ALIAS is empty'
            });
        };
        //11
        if(req.body.alias === exchangeRates.usd.alias || 
            req.body.alias === exchangeRates.yen.alias || 
            req.body.alias === exchangeRates.peso.alias ||
            req.body.alias === exchangeRates.yuan.alias ||
            req.body.alias === exchangeRates.won.alias){
            return res.status(400).send({
                'error': 'Bad request: required parameter ALIAS is a duplicate'
            });
        };
        //12
        if((req.body.name.length !== 0 && req.body.alias.length !== 0 && 
            Object.keys(req.body.ex).length === 0) &&            
            req.body.alias === exchangeRates.usd.alias || 
            req.body.alias === exchangeRates.yen.alias || 
            req.body.alias === exchangeRates.peso.alias ||
            req.body.alias === exchangeRates.yuan.alias ||
            req.body.alias === exchangeRates.won.alias){
            return res.status(200).send('Success');
        };
        //1
        if(req.body !== undefined){
            return res.status(200).send('Endpoint is working');
        };
    });
};