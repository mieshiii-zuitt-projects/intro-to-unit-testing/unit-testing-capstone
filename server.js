const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

require('./app/routes')(app, {});

app.listen(port, () => console.log(`Server running at port ${port}`));