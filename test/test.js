const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');

chai.use(http);

describe('TDD Capstone', () => {
    //1
    it('Test that Post /currency is working', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'string',
            'name': 'string',
            'ex': {
                'peso': 50.73
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    });
    //2
    it('Test that Post /currency return status 400 if name is missing', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            });
    });
    //3
    it('Test that Post /currency return status 400 if name is not string', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': 5,
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //4
    it('Test that Post /currency return status 400 if name is an empty string', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': '',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //5
    it('Test that Post /currency return status 400 if ex is missing', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': 'asd'
        })
        .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
    });
    //6
    it('Test that Post /currency return status 400 if ex is not an object', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': 'asd',
            'ex': 'asd'
        })
        .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
    });
    //7
    it('Test that Post /currency return status 400 if ex is an empty object', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': 'asd',
            'ex': {}
        })
        .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
    });
    //8
    it('Test that Post /currency return status 400 if alias is missing', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'name': 'asd',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
        });
    });
    //9
    it('Test that Post /currency return status 400 if alias is not string', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 5,
            'name': 'United States Dollar',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //10
    it('Test that Post /currency return status 400 if alias is an empty string', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : '',
            'name': 'United States Dollar',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });
    //11
    it('Test that Post /currency return status 400 if all fields are complete but there is a duplicate alias', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'dollars',
            'name': 'United States Dollar',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    });
    //12
    it('Test that Post /currency return status 200 if all fields are complete but there is no duplicate', (done) => {
        chai.request('http://localhost:5000')
        .post('/currency')
        .type('json')
        .send({
            'alias' : 'pounds',
            'name': 'Great Britain Pounds',
            'ex': {
                'peso': 50.73,
                'won': 1187.24,
                'yen': 108.63,
                'yuan': 7.03,
                'dollars': 1.33
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    });
});